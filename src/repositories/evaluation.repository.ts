import { Evaluation } from '@definitions/evaluation';
import { BaseRepository } from './base.repository';

export default class EvaluationRepository extends BaseRepository<Evaluation> {}
