import { UserBehaviour } from '@definitions/UserBehaviour';
import { BaseRepository } from './base.repository';

export class UserBehaviourRepository extends BaseRepository<UserBehaviour> {}
