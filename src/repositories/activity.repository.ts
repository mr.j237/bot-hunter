import { Activity } from '@definitions/Activity';
import { BaseRepository } from './base.repository';

export class ActivityRepository extends BaseRepository<Activity> {}
