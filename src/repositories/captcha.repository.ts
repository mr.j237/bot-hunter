import { Captcha } from '@definitions/Captcha';
import { BaseRepository } from './base.repository';

export default class CaptchaRepository extends BaseRepository<Captcha> {}
