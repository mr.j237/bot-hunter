import { Visitor } from '@definitions/Visitors';
import { BaseRepository } from './base.repository';

export default class VisitorRepository extends BaseRepository<Visitor> {}
